# Java - File system manager
Small console project that displays a menu to the user, which provides them with info of files contained in the /resource folder. In addition the user can get general info about the Dracula.txt file in /resources and search for words within that file as well.

When doing commands on the Dracula.txt file, the program will also log the results in a seperate log.txt file (/resources/) with its timestamp and function execution time.

## Assumptions
* I assume we only need to print the base message in the console, and in the log add the timestamp and execution time. 
* I also assume that the first two functions do not require to be logged at all.
* I also assume that we only need to show the name of the .txt file to the user (and not let them change the file name).

## Showcase

### javac command to compile all files into /out/
![](gitlab-files/showcase1.PNG)

### Create single .jar file in /out/
![](gitlab-files/showcase2.PNG)

### Running .jar file and start of program
![](gitlab-files/showcase3.PNG)

### List all files in '/resources'
![](gitlab-files/showcase4.PNG)

### Get files in '/resources' by extension
![](gitlab-files/showcase5.PNG)

### Get name of text file
![](gitlab-files/showcase6.PNG)

### Get size of file (in KB)
![](gitlab-files/showcase7.PNG)

### Get line count of file
![](gitlab-files/showcase8.PNG)

### Search for a specific word in file
![](gitlab-files/showcase9.PNG)

### Get how often a specific word appears in file
![](gitlab-files/showcase10.PNG)