package loggingservice;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LoggingService {
    private final String LOG_PATH = ".\\resources\\log.txt";

    public void logMessage(String message, long time) {
        try {
            File logFile = new File(LOG_PATH);
            logFile.createNewFile();

            FileWriter writer = new FileWriter(LOG_PATH, true);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write(getNowTime() + ": " + message + ". The function took " + time + "ms to complete.\n");
            buffer.close();
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    /**
     * Print out formatted time right now
     */
    private String getNowTime() {
        LocalDateTime localDateTimeNow = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        return dtf.format(localDateTimeNow);
    }

}
