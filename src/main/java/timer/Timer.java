package timer;

/**
 * A timer you can start and stop and get the total time in milliseconds
 * in-between
 */
public class Timer {
    private long startTime = 0;
    private long endTime = 0;

    public void startTimer() {
        startTime = System.currentTimeMillis();
    }

    public void endTimer() {
        endTime = System.currentTimeMillis();
    }

    public void reset() {
        startTime = endTime = 0;
    }

    public long getMilliseconds() {
        return Math.abs(endTime - startTime);
    }
}
