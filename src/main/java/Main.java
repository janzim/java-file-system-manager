import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

import fileservice.FileService;
import loggingservice.LoggingService;
import menu.Menu;
import timer.Timer;

/**
 * ASSUMPTIONS: I assume you only want us to print the base message in console,
 * and in the log add the timestamp and execution time. I also assume that the
 * first two functions do not require to be logged at all. I also assume that we
 * only need to show the name of the .txt file to the user (and not let them
 * change the file name)
 */

public class Main {
    public static void main(String[] args) {
        Menu menu = new Menu();
        FileService fs = new FileService();
        Scanner sc = new Scanner(System.in);
        LoggingService log = new LoggingService();
        Timer timer = new Timer();
        int choice;
        String baseMessage = "";

        while (true) {
            choice = menu.displayMenu();

            switch (choice) {
                // List all files
                case 1:
                    File[] allFiles = fs.getAllFiles();
                    Arrays.stream(allFiles).forEach(f -> System.out.print(f.getName() + " "));
                    break;
                // get all files by extension
                case 2:
                    // todo let user write in extension
                    String extension = getWordInput("Extension: ");
                    File[] extFiles = fs.getFilesByExtension(extension);
                    if (extFiles.length > 0) {
                        baseMessage = "Displaying all files with 'jpg' extension: ";
                        Arrays.stream(extFiles).forEach(f -> System.out.print(f.getName() + " "));
                    }
                    break;
                // get file name of .txt
                case 3:
                    timer.startTimer();
                    String fileName = fs.getTxtFileName();
                    timer.endTimer();
                    baseMessage = "Name of .txt file is " + fileName;
                    break;
                // get file size of .txt
                case 4:
                    timer.startTimer();
                    int fileSize = fs.getTxtFileSize();
                    timer.endTimer();
                    baseMessage = "Size of .txt file is " + fileSize / 1024.0 + " kilobytes ("
                            + (int) Math.ceil(fileSize / 1024.0) + "KB on disk). ";
                    break;

                // get line count of .txt
                case 5:
                    timer.startTimer();
                    long fileLinesCount = fs.getTxtFileLineCount();
                    timer.endTimer();

                    baseMessage = "The amount of lines is " + fileLinesCount;
                    break;
                // check if specific word exists in .txt
                case 6:
                    String word = getWordInput("Word: ");

                    timer.startTimer();
                    boolean wordExists = fs.doesWordExistInTxtFile(word);
                    timer.endTimer();

                    if (wordExists)
                        baseMessage = "Word '" + word + "' exists in .txt file.";
                    else
                        baseMessage = "Word '" + word + "' does not exist in .txt file.";

                    break;
                // get occurences of word in .txt
                case 7:
                    word = getWordInput("Word: ");

                    timer.startTimer();
                    int occurences = fs.getOccurencesInTxtFile(word);
                    timer.endTimer();

                    baseMessage = "The word '" + word + "' occurs " + occurences + " times in the .txt file. ";
                    break;
                default: {
                    System.out.println("Something went wrong :/");
                    break;
                }
            }

            // only log the "file manipulation" functions
            if (choice >= 3 && choice <= 7) {
                System.out.println(baseMessage);
                log.logMessage(baseMessage, timer.getMilliseconds());
            }

            System.out.print("\nPress 'Enter' button to continue...");
            timer.reset();
            sc.nextLine();
        }
    }

    /**
     * Give user a prompt and read input
     * 
     * @param prompt text to show to user prior to input
     * @return user input string
     */
    public static String getWordInput(String prompt) {
        Scanner sc = new Scanner(System.in);
        System.out.print(prompt);
        String word = sc.nextLine();
        return word;
    }

}