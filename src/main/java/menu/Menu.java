package menu;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {
    private final String menuTxt = "\n" + "#".repeat(23) + "\n# FILE SYSTEM MANAGER #\n" + "#".repeat(23) + "\n\n"
            + "Choose an option:\n" + "(1) List all files in '/resources'\n"
            + "(2) Get files in '/resources' by extension\n" + "## MANIPULATE DRACULA.TXT ##\n"
            + "(3) Get name of text file\n" + "(4) Get size of file (in KB)\n" + "(5) Get line count of file\n"
            + "(6) Search for a specific word in file\n" + "(7) Get how often a specific word appears in file\n"
            + "q. quit\n\n" + "Choice: ";

    /**
     * Display the menu and do so until user quits
     * 
     * @return integer of menu choice
     */
    public int displayMenu() {
        Scanner sc = new Scanner(System.in);
        int value = -1;

        do {
            try {
                System.out.print(menuTxt);
                value = sc.nextInt();
            } catch (InputMismatchException e) {
                if (sc.next().equals("q")) {
                    System.out.println("Exiting...");
                    System.exit(0);
                }
            }
        } while (value < 1 || value > 7);

        System.out.println("");

        return value;
    }
}
