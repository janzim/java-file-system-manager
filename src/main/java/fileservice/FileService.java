package fileservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class FileService {
    private final String RESOURCE_PATH = ".\\resources";
    private static final File[] NO_FILES = {};

    /**
     * Check if the resource folder exists
     * 
     * @return true if folder exists, false otherwise
     */
    private boolean doesFolderExist() {
        try {
            File folder = new File(RESOURCE_PATH);

            return folder.exists();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get all files in the project "resource" folder
     * 
     * @return Array of all files in folder
     */
    public File[] getAllFiles() {
        if (doesFolderExist()) {
            File folder = new File(RESOURCE_PATH);
            File[] allFiles = folder.listFiles();
            return allFiles;
        } else {
            System.out.println("Folder '" + RESOURCE_PATH + "' was not found!");
        }
        return NO_FILES;
    }

    /**
     * Get all files with the specific extension from parameter
     * 
     * @param extensionName The extension to search for files after (.png, .txt,
     *                      ...)
     * @return Array of all files with extensionName
     */
    public File[] getFilesByExtension(final String extensionName) {
        if (doesFolderExist()) {
            File folder = new File(RESOURCE_PATH);
            File[] allFiles = folder.listFiles((dir, name) -> name.toLowerCase().endsWith(extensionName));
            if (allFiles.length > 0) {
                return allFiles;
            } else {
                System.out.println(
                        "There are no files in folder '" + RESOURCE_PATH + "' with extension '" + extensionName + "'.");
            }
        } else {
            System.out.println("Folder '" + RESOURCE_PATH + "' was not found!");
        }
        return NO_FILES;
    }

    /**
     * Get the text file provided in folder
     * 
     * @return filename of text file
     */
    public String getTxtFileName() {
        if (doesFolderExist()) {
            File folder = new File(RESOURCE_PATH);
            File[] draculaFile = folder.listFiles((dir, name) -> name.equals("Dracula.txt"));

            if (draculaFile.length > 0) {
                return draculaFile[0].getName();
            }
        } else {
            return "Folder '" + RESOURCE_PATH + "' was not found!";
        }

        return "Text file not found";
    }

    /**
     * Get the size of the file in bytes
     * 
     * @return filesize in bytes
     */
    public int getTxtFileSize() {
        try (FileInputStream fileInputStream = new FileInputStream(RESOURCE_PATH + "\\Dracula.txt")) {
            int data = fileInputStream.read();
            int byteCount = 0;

            while (data != -1) {
                byteCount++;
                data = fileInputStream.read();
            }

            return byteCount;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public long getTxtFileLineCount() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(RESOURCE_PATH + "\\Dracula.txt"))) {
            return bufferedReader.lines().count();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    /**
     * Check if a word exists in the .txt file
     * 
     * @param word the word to match
     * @return true if word exists, false otherwise
     */
    public boolean doesWordExistInTxtFile(String word) {
        final String finalWord = word.toLowerCase();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(RESOURCE_PATH + "\\Dracula.txt"))) {
            String line;
            // match parameter word with .txt words line for line regardless of lower/upper
            // case
            while ((line = bufferedReader.readLine()) != null) {
                if (Arrays.stream(line.toLowerCase().replaceAll("[^A-Za-z0-9]", " ").split(" "))
                        .anyMatch(w -> w.equals(finalWord))) {
                    return true;
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
        return false;
    }

    /**
     * Get the amount of occurences of a specific word in the text file
     * 
     * @param word the word to search for
     * @return amount of occurences
     */
    public int getOccurencesInTxtFile(String word) {
        int count = 0;
        final String finalWord = word.toLowerCase();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(RESOURCE_PATH + "\\Dracula.txt"))) {
            String line;
            // match parameter word with .txt words line for line regardless of lower/upper
            // case and add to our count
            while ((line = bufferedReader.readLine()) != null) {
                count += Arrays.stream(line.toLowerCase().replaceAll("[^A-Za-z0-9]", " ").split(" "))
                        .filter(w -> w.equals(finalWord)).count();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return 0;
        }

        return count;
    }

}
